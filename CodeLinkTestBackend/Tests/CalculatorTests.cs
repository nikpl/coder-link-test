using CodeLinkTestBackend.Controllers;
using CodeLinkTestBackend.Model.Calculator;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace Tests
{
    public class CalculatorTests
    {
        [Fact]
        public void SumOkTest()
        {
            var requestPayload = new BinaryOperationData() { LeftValue = "1", RightValue = "2" };
            var result = RunSumWithPayload(requestPayload);
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
            Assert.IsType<OperationResultData>(okResult.Value);
            var resultValue = okResult.Value as OperationResultData;
            Assert.Equal("3", resultValue.Result);
        }

        [Fact]
        public void SumErrorTest()
        {
            var requestPayload = new BinaryOperationData() { LeftValue = "one", RightValue = "2" };
            var result = RunSumWithPayload(requestPayload);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequestResult = result as BadRequestObjectResult;
            Assert.IsType<OperationErrorData>(badRequestResult.Value);
        }

        private IActionResult RunSumWithPayload(BinaryOperationData requestPayload)
        {
            var calculatorController = new CalculatorController();
            return calculatorController.Sum(requestPayload);
        }
    }
}
