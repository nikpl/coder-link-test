﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeLinkTestBackend.Model.Calculator
{
    public class CalculatorOperationException: Exception
    {
        public CalculatorOperationException(string userMessage): base(userMessage)
        {

        }
    }
}
