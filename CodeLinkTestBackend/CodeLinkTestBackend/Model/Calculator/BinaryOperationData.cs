﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeLinkTestBackend.Model.Calculator
{
    public class BinaryOperationData
    {
        public string LeftValue { get; set; }
        public string RightValue { get; set; }
    }
}
