﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeLinkTestBackend.Model.Calculator
{
    public class OperationErrorData
    {
        public string Message { get; set; }
    }
}
