﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeLinkTestBackend.Model.Calculator;

namespace CodeLinkTestBackend.Controllers
{
    [Route("api/[controller]")]
    public class CalculatorController : Controller
    {
        [HttpPost("sum")]
        public IActionResult Sum([FromBody]BinaryOperationData data)
        {
            try
            {
                var result = MakeSum(data.LeftValue, data.RightValue);
                return Ok(new OperationResultData { Result = result });
            }
            catch (CalculatorOperationException e)
            {
                return OnOperationError(e.Message);
            }
        }

        private string MakeSum(string leftValueStr, string rightValueStr)
        {
            if (!Decimal.TryParse(leftValueStr, out var leftValue))
                throw new CalculatorOperationException("Недопустимое значение слева");
            if (!Decimal.TryParse(rightValueStr, out var rightValue))
                throw new CalculatorOperationException("Недопустимое значение справа");
            var result = leftValue + rightValue;
            return result.ToString();
        }

        private IActionResult OnOperationError(string message)
        {
            return BadRequest(new OperationErrorData { Message = message });
        }
    }
}
