### Описание

Здесь представлено моё решение тестового задания от Coderlink.

Оно состоит из двух проектов:
 - Бэкэнда на C#, ASP.Core WebApi
 - Фонтэнда на Angular 6

#### Бэкэнд

[Единственный контроллер, который и реализует сложение.](CodeLinkTestBackend/CodeLinkTestBackend/Controllers/CalculatorController.cs)

Сервер принимает post-запрос, в теле которого должен быть json-объект со свойствами 
leftValue и rightValue, которые являются операндами сложения. Если одно из значений 
не является числом, возвращает ошибку с описанием.

#### Фронтэнд

- Компонент с полями ввода:
  - [код](coder-link-test-front/src/app/app.component.ts) 
  - [разметка](coder-link-test-front/src/app/app.component.html)
- [Сервис, который отправляет запрос на сервер и обрабатывает результат](coder-link-test-front/src/app/calculator.service.ts)

Компонент передаёт введённые пользователем данные в сервис. Сервис формирует и 
отпавляет запрос. Результат запроса, в том числе ошибка, трансформируется в 
понятный пользователю текст с помощью операторов ReactiveX.

### Запуск

Проект Бэкэнда можно запустить в режиме отладки в Visual Studio (для разработки использовалась VS 2017)

Для запуска фронтэнда:
 - установить Angular cli: npm -g install @angular/cli
 - из каталога проекта выполнить: ng serve --proxy.conf.json
 - открыть localhost:4200