import {Component, OnInit} from '@angular/core';
import {CalculatorService} from './calculator.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  leftValue = '0';
  rightValue = '0';
  result = '';

  // Почему для работы с кнопкой используется Subject/Observable:
  // такой подход позволяет довольно просто и "в стиле Reactive"
  // отписаться от текущего запроса, если он есть, при повторном
  // нажатии кнопки, что в свою очередь, позволяет избежать неожиданных
  // результатов, когда пользователь делает новый запрос не дождавшись
  // ответа на предыдущий.
  // Конечно, вместо этого можно было сохранить Subscription и
  // использовать для отписки её - всё зависит от соглашений внутри команды.
  buttonClickSubject = new Subject();
  buttonClick = this.buttonClickSubject.asObservable();

  onButtonClick() {
    this.buttonClickSubject.next();
  }

  constructor(private calculator: CalculatorService) {
    this.subscribeSubmitToButtonClick();
  }

  private subscribeSubmitToButtonClick() {
    this.buttonClick.subscribe(() => this.submit());
  }

  submit() {
    this.result = 'Подождите...';
    this.calculator.sum(this.leftValue, this.rightValue)
      .pipe(takeUntil(this.buttonClick)) // здесть происходит прерывание текущего запроса при повоторном нажатии на кнопку
      .subscribe(val => this.result = val);
  }
}
