export interface BinaryOperationData {
  leftValue: string;
  rightValue: string;
}

export interface OperationOkResult {
  result: string;
}
