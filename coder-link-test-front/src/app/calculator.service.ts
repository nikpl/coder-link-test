import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {BinaryOperationData, OperationOkResult} from './model';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  private static readonly baseUrl = '/api/calculator/';

  constructor(private http: HttpClient) {
  }

  sum(leftValue: string, rightValue: string): Observable<string> {
    return this.binaryOperation('sum', {leftValue: leftValue, rightValue: rightValue});
  }

  private binaryOperation(operation: string, operationData: BinaryOperationData): Observable<string> {
    const url = this.getUrl(operation);
    return this.http.post<OperationOkResult>(url, operationData)
      .pipe(
        map(r => 'Результат: ' + r.result),
        catchError(e => this.mapErrorToMessage(e))
      );
  }

  private getUrl(operation: string) {
    return CalculatorService.baseUrl + operation;
  }

  private mapErrorToMessage(e: any) {
    let message = '';
    if (e.error && e.error.message) {
      message = 'Ошибка: ' + e.error.message;
    } else {
      message = 'Неизвестная ошибка';
      if (e.message) {
        message += ': ' + e.message;
      }
    }
    return of(message);
  }
}
